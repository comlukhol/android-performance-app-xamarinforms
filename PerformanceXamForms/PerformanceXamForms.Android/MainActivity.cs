﻿using Android.App;
using Android.Content.PM;
using Android.Util;
using Android.OS;
using Xamarin.Forms;
using Android;
using Plugin.Permissions;
using Android.Runtime;

namespace PerformanceXamForms.Droid
{
    [Activity(Label = "Performance XamForms", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);
            Plugin.CurrentActivity.CrossCurrentActivity.Current.Init(this, bundle);

            var activity = (Activity)Forms.Context;
            var myExtraString = activity.Intent.GetStringExtra("myExtra");
            var serverUrlString = activity.Intent.GetStringExtra("serverUrl");
            LoadApplication(new App(myExtraString, serverUrlString));
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}

