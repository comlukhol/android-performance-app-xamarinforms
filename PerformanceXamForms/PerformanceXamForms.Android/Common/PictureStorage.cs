﻿using System;
using System.IO;
using System.Threading.Tasks;
using Android.Content;
using PerformanceXamForms.Common;
using PerformanceXamForms.Droid.Common;

[assembly: Xamarin.Forms.Dependency(typeof(PictureStorage))]
namespace PerformanceXamForms.Droid.Common
{
    public class PictureStorage : IPictureStorage
    {
        public async Task SavePictureAsync(string filename, byte[] imageBytes)
        {
            try
            {
                File.WriteAllBytes(filename, imageBytes);
            }
            catch (System.Exception e)
            {
                System.Console.WriteLine(e.ToString());
            }
        }
    }
}