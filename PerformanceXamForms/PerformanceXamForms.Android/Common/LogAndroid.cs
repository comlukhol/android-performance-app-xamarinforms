﻿using PerformanceXamForms.Common;
using PerformanceXamForms.Droid.Common;
using Xamarin.Forms;
using Android.Util;

[assembly: Dependency(typeof(LogAndroid))]
namespace PerformanceXamForms.Droid.Common
{
    public class LogAndroid : ILog
    {
        public void Debug(string TAG, string message)
        {
            Log.Debug(TAG, message);
        }

        public void Error(string TAG, string message)
        {
            Log.Error(TAG, message);
        }

        public void Info(string TAG, string message)
        {
            Log.Info(TAG, message);
        }

        public void Verbose(string TAG, string message)
        {
            Log.Verbose(TAG, message);
        }

        public void Warn(string TAG, string message)
        {
            Log.Warn(TAG, message);
        }

        public void EmmitFinish()
        {
            Info("performance-tests-ended", "performance-tests-ended");
        }
    }
}