﻿using PerformanceXamForms.Common;
using PerformanceXamForms.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PerformanceXamForms.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ReadFromDevicePage : ContentPage
	{
        private readonly INavigationService navigationService;
        private readonly ReadFromDeviceViewModel readFromDeviceViewModel;

        public ReadFromDevicePage(INavigationService navigationService, ReadFromDeviceViewModel readFromDeviceViewModel)
        {
            this.navigationService = navigationService;
            this.readFromDeviceViewModel = readFromDeviceViewModel;

            InitializeComponent();
            BindingContext = readFromDeviceViewModel;
        }
    }
}