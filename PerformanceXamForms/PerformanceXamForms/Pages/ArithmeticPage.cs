﻿using PerformanceXamForms.Common;
using PerformanceXamForms.Pages.Base;
using PerformanceXamForms.ViewModels;

namespace PerformanceXamForms.Pages
{
	public class ArithmeticPage : BaseTestPage
	{
        public ArithmeticPage(INavigationService navigationService, ArithmeticViewModel arithmeticViewModel) 
            : base(navigationService, arithmeticViewModel)
        {
            Title = "Arithmetic";
        }
    }
}