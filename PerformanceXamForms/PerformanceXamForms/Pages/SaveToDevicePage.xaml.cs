﻿using PerformanceXamForms.Common;
using PerformanceXamForms.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PerformanceXamForms.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SaveToDevicePage : ContentPage
	{
        private readonly INavigationService navigationService;
        private readonly SaveToDeviceViewModel saveToDeviceViewModel;

        public SaveToDevicePage(INavigationService navigationService, SaveToDeviceViewModel saveToDeviceViewModel)
        {
            this.navigationService = navigationService;
            this.saveToDeviceViewModel = saveToDeviceViewModel;

            InitializeComponent();
            BindingContext = saveToDeviceViewModel;
        }
    }
}