﻿using PerformanceXamForms.Common;
using PerformanceXamForms.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PerformanceXamForms.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class JsonDeserializationPage : ContentPage
	{
        private readonly INavigationService navigationService;
        private readonly JsonDeserializationViewModel jsonDeserializationViewModel;

        public JsonDeserializationPage (INavigationService navigationService, JsonDeserializationViewModel jsonDeserializationViewModel)
		{
            this.navigationService = navigationService;
            this.jsonDeserializationViewModel = jsonDeserializationViewModel;

            InitializeComponent ();
            BindingContext = jsonDeserializationViewModel;
		}
	}
}