﻿using PerformanceXamForms.Common;
using PerformanceXamForms.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PerformanceXamForms.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LocationPage : ContentPage
	{
        private readonly INavigationService navigationService;
        private readonly LocationViewModel locationViewModel;

        public LocationPage(INavigationService navigationService, LocationViewModel locationViewModel)
        {
            this.navigationService = navigationService;
            this.locationViewModel = locationViewModel;

            InitializeComponent();
            BindingContext = locationViewModel;
        }
	}
}