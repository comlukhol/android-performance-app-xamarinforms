﻿using PerformanceXamForms.Common;
using PerformanceXamForms.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PerformanceXamForms.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DBReadAllPage : ContentPage
	{
        private readonly INavigationService navigationService;
        private readonly DBReadAllViewModel dbReadAllViewModel;

        public DBReadAllPage(INavigationService navigationService, DBReadAllViewModel dbReadAllViewModel)
        {
            this.navigationService = navigationService;
            this.dbReadAllViewModel = dbReadAllViewModel;

            InitializeComponent();
            BindingContext = dbReadAllViewModel;
        }
    }
}