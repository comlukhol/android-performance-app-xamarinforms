﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PerformanceXamForms.Pages
{
    public class MasterPageItem
    {
        public string Title { get; private set; }
        public Type TargetType { get; private set; }
        public string IconSource { get; private set; }

        public MasterPageItem(string title, Type targetType, string iconSource)
        {
            Title = title;
            TargetType = targetType;
            IconSource = iconSource;
        }
    }
}