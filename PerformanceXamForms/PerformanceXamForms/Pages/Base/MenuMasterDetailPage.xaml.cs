﻿using PerformanceXamForms.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PerformanceXamForms.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MenuMasterDetailPage : ContentPage
	{
        public ListView ListView { get; set; }

        public MenuMasterDetailPage(MenuMasterDetailViewModel menuMasterDetailViewModel)
        {
            InitializeComponent();
            BindingContext = menuMasterDetailViewModel;
            ListView = ListViewXaml;
        }
    }
}