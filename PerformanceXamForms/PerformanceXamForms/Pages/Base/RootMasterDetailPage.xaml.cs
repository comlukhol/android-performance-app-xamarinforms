﻿using PerformanceXamForms.Common;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PerformanceXamForms.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RootMasterDetailPage : MasterDetailPage
	{
        private MenuMasterDetailPage menuPage;
        private INavigationService navigationService;

		public RootMasterDetailPage(MenuMasterDetailPage masterPage, INavigationService navigationService)
		{
            this.navigationService = navigationService ?? throw new ArgumentNullException(nameof(navigationService));

			InitializeComponent();
            Master = masterPage;
            menuPage = masterPage;

            masterPage.ListView.ItemSelected += OnMenuItemSelected;
		}

        private async void OnMenuItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as MasterPageItem;
            if (item != null)
            {
                menuPage.ListView.SelectedItem = null;
                await navigationService.NavigateTo(item.TargetType);
            }

            IsPresented = false;
        }
    }
}