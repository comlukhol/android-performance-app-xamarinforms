﻿using PerformanceXamForms.Common;
using PerformanceXamForms.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PerformanceXamForms.Pages.Base
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public abstract partial class BaseTestPage : ContentPage
	{
        private readonly INavigationService navigationService;
        private readonly BaseTestViewModel baseTestViewModel;

        public BaseTestPage(INavigationService navigationService, BaseTestViewModel baseTestViewModel)
		{
            this.navigationService = navigationService;
            this.baseTestViewModel = baseTestViewModel;
            BindingContext = baseTestViewModel;
			InitializeComponent();
		}
	}
}