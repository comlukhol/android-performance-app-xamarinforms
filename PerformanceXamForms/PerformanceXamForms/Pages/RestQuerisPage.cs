﻿using PerformanceXamForms.Common;
using PerformanceXamForms.Pages.Base;
using PerformanceXamForms.ViewModels;

namespace PerformanceXamForms.Pages
{
	public class RestQuerisPage : BaseTestPage
	{
        public RestQuerisPage(INavigationService navigationService, RestQuerisViewModel restQuerisViewModel)
            : base(navigationService, restQuerisViewModel)
        {
            Title = "Rest queris";
        }
    }
}