﻿using PerformanceXamForms.Common;
using PerformanceXamForms.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PerformanceXamForms.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DBBulkInsertPage : ContentPage
	{
        private readonly INavigationService navigationService;
        private readonly DBBulkInsertViewModel dbBulkInsertViewModel;

        public DBBulkInsertPage(INavigationService navigationService, DBBulkInsertViewModel dbBulkInsertViewModel)
        {
            this.navigationService = navigationService;
            this.dbBulkInsertViewModel = dbBulkInsertViewModel;

            InitializeComponent();
            BindingContext = dbBulkInsertViewModel;
        }
    }
}