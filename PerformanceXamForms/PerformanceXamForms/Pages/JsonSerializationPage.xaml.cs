﻿using PerformanceXamForms.Common;
using PerformanceXamForms.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PerformanceXamForms.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class JsonSerializationPage : ContentPage
	{
        private readonly INavigationService navigationService;
        private readonly JsonSerializationViewModel jsonSerializationViewModel;

        public JsonSerializationPage(INavigationService navigationService, JsonSerializationViewModel jsonSerializationViewModel)
		{
            this.navigationService = navigationService;
            this.jsonSerializationViewModel = jsonSerializationViewModel;

			InitializeComponent();
            BindingContext = jsonSerializationViewModel;
		}
    }
}