﻿namespace PerformanceXamForms.Model.Dto
{
    public class WeatherDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int Cod { get; set; }
        public Coord Coord { get; set; }
    }
}