﻿namespace PerformanceXamForms.Model.Dto
{
    public class InnerJsonSample
    {
        public string Id { get; set; }
        public int Value { get; set; }
    }
}