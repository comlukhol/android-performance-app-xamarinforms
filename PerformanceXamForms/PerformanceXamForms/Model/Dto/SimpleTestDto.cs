﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace PerformanceXamForms.Model.Dto
{
    public class SimpleTestDto
    {
        private const string applicationType = "XAMARIN_FORMS";

        public string TestId { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public TestCode TestCode { get; set; }
        public string ApplicationType => applicationType;
        public long time;
    }
}