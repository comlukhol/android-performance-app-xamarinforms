﻿using System.Collections.Generic;

namespace PerformanceXamForms.Model.Dto
{
    public class JsonSerializationSample
    {
        public long Id { get; set; }
        public string Content { get; set; }
        public IList<string> Topics { get; set; }
        public bool Success { get; set; }
        public double Value { get; set; }
        public InnerJsonSample InnerJsonSample { get; set; }
    }
}