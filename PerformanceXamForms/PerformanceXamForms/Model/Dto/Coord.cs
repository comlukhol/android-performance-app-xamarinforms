﻿namespace PerformanceXamForms.Model.Dto
{
    public class Coord
    {
        double Lon { get; set; }
        double Lat { get; set; }
    }
}