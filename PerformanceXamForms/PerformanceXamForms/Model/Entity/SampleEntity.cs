﻿using SQLite;
using System;

namespace PerformanceXamForms.Model.Entity
{
    [Table("sample_entity")]
    public class SampleEntity
    {
        [PrimaryKey, AutoIncrement]
        public long Id { get; set; }

        public String StringContent { get; set; }
        public bool BooleanContent { get; set; }
        public double DoubleContent { get; set; }
    }
}
