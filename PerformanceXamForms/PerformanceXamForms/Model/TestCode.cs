﻿namespace PerformanceXamForms.Model
{
    public enum TestCode
    {
        ARITHMETIC,
        JSON_SERIALIZATION,
        JSON_DESERIALIZATION,
        REST_QUERIES,
        READ_FROM_DEVICE,
        SAVE_TO_DEVICE,
        SAVE_TO_DATABASE,
        LOAD_FROM_DATABASE,
        TRANSLATION_SIMPLE_PAGE,
        TRANSLATION_TO_MAP_PAGE,

        READ_LAT_LNG
    }
}