﻿using System;

namespace PerformanceXamForms.Model.State
{
    public class TestState
    {
        public TestCode TestCode { get; set; }
        public bool IsBusy { get; set; }
        public bool IsSuccess { get; set; }
        public Tuple<long, string> Result { get; set; }

        public TestState(TestCode testCode, bool isBusy, bool isSuccess, Tuple<long, string> result)
        {
            TestCode = testCode;
            IsBusy = isBusy;
            IsSuccess = isSuccess;
            Result = result;
        }
    }
}