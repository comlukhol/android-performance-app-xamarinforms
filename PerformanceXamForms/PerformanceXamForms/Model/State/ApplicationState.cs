﻿using System;
using System.Collections.Generic;

namespace PerformanceXamForms.Model.State
{
    public class ApplicationState
    {
        public string TestId { get; private set; }
        public IDictionary<TestCode, TestState> TestStatesDictionary { get; private set; }

        public ApplicationState() { }

        public ApplicationState(string testId, IDictionary<TestCode, TestState> testStatesDictionary)
        {
            TestId = testId;
            TestStatesDictionary = testStatesDictionary;
        }
    }
}
