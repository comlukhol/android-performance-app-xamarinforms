﻿using PerformanceXamForms.Model.State;
using Redux;
using PerformanceXamForms.Model;
using PerformanceXamForms.Redux.ActionCreators;

namespace PerformanceXamForms.ViewModels
{
    public class ArithmeticViewModel : BaseTestViewModel
    {
        public ArithmeticViewModel(IStore<ApplicationState> appStore, ArithmeticActionCreator arithmeticActionCreator) 
            : base(appStore, arithmeticActionCreator, TestCode.ARITHMETIC)
        {
            TestDescription = "Test prostych operacji arytmetycznych w pętli for, która wykonuje 10 milionów iteracji. Pętla dodaje 1 do zmiennej suma przy każdej iteracji.";
        }
    }
}