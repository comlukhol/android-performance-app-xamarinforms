﻿using PerformanceXamForms.Model;
using PerformanceXamForms.Model.State;
using PerformanceXamForms.Redux.ActionCreators;
using Redux;
using System;
using System.Reactive.Linq;

namespace PerformanceXamForms.ViewModels
{
    public class JsonSerializationViewModel : BaseTestViewModel
    {
        public JsonSerializationViewModel(IStore<ApplicationState> appStore, JsonSerializationActionCreator jsonSerializationActionCreator) 
            : base(appStore, jsonSerializationActionCreator, TestCode.JSON_SERIALIZATION)
        {
            TestDescription = "Test serializacji obiektów do Stringa w postaci Jsona. Ten sam obiekt jest serializowany 10 000 razy. Użyta biblioteka to Json Newtonsoft.";
        }
    }
}