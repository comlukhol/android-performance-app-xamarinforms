﻿using PerformanceXamForms.Model;
using PerformanceXamForms.Model.State;
using PerformanceXamForms.Redux.ActionCreators;
using Redux;

namespace PerformanceXamForms.ViewModels
{
    public class SaveToDeviceViewModel : BaseTestViewModel
    {
        public SaveToDeviceViewModel(IStore<ApplicationState> appStore, SaveToDeviceActionCreator saveToDeviceActionCreator) 
            : base(appStore, saveToDeviceActionCreator, TestCode.SAVE_TO_DEVICE)
        {
            TestDescription = "Save image to device storage.";
        }
    }
}
