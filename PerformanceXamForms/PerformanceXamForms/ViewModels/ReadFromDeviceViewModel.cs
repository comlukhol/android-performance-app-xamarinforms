﻿using PerformanceXamForms.Model;
using PerformanceXamForms.Model.State;
using PerformanceXamForms.Redux.ActionCreators;
using Redux;

namespace PerformanceXamForms.ViewModels
{
    public class ReadFromDeviceViewModel : BaseTestViewModel
    {
        public ReadFromDeviceViewModel(IStore<ApplicationState> appStore, ReadFromDeviceActionCreator readFromDeviceActionCreator)
            : base(appStore, readFromDeviceActionCreator, TestCode.READ_FROM_DEVICE)
        {
            TestDescription = "Ten test wczytuje zdjęcie z pamięci urządzenia, a następnie zamienia je na tablicę bajtów. Ta sama operacja wykonana jest 100 razy pod rząd.";
        }
    }
}
