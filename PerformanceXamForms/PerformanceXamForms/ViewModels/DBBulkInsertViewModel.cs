﻿using PerformanceXamForms.Model;
using PerformanceXamForms.Model.State;
using PerformanceXamForms.Redux.ActionCreators;
using Redux;

namespace PerformanceXamForms.ViewModels
{
    public class DBBulkInsertViewModel : BaseTestViewModel
    {
        public DBBulkInsertViewModel(IStore<ApplicationState> appStore, DBBulkInsertActionCreator DBBulkInsertActionCreator)
            : base(appStore, DBBulkInsertActionCreator, TestCode.SAVE_TO_DATABASE)
        {
            TestDescription = "DB bulk insert";
        }
    }
}
