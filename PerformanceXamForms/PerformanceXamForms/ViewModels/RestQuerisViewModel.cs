﻿using PerformanceXamForms.Model;
using PerformanceXamForms.Model.State;
using PerformanceXamForms.Redux.ActionCreators;
using Redux;

namespace PerformanceXamForms.ViewModels
{
    public class RestQuerisViewModel: BaseTestViewModel
    {
        public RestQuerisViewModel(IStore<ApplicationState> appStore, RestQuerisActionCreator restQuerisActionCreator)
            : base(appStore, restQuerisActionCreator, TestCode.REST_QUERIES)
        {
            TestDescription = "Zapytania REST";
        }
    }
}