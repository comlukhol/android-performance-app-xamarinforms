﻿using PerformanceXamForms.Model;
using PerformanceXamForms.Model.State;
using PerformanceXamForms.Redux;
using PerformanceXamForms.Redux.ActionCreators;
using Redux;
using System;
using System.ComponentModel;
using System.Reactive.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace PerformanceXamForms.ViewModels
{
    public abstract class BaseTestViewModel : INotifyPropertyChanged
    {
        protected IStore<ApplicationState> appStore;
        protected BaseTestActionCreator baseTestActionCreator;
        protected TestCode testCode;

        public event PropertyChangedEventHandler PropertyChanged;
        public ICommand StartTestCommand => new Command(ExecuteTest);

        public BaseTestViewModel(IStore<ApplicationState> appStore, BaseTestActionCreator baseTestActionCreator, TestCode testCode)
        {
            this.appStore = appStore ?? throw new ArgumentNullException(nameof(appStore));
            this.baseTestActionCreator = baseTestActionCreator ?? throw new ArgumentNullException(nameof(baseTestActionCreator));
            this.testCode = testCode;

            appStore
                .DistinctUntilChanged(state => new { state.TestStatesDictionary })
                .Subscribe(state =>
                {
                    var arithmeticTestState = state.TestStatesDictionary[testCode];

                    IsBusy = arithmeticTestState.IsBusy;
                    if (arithmeticTestState.Result == null)
                    {
                        Result = "";
                        ExecutionTime = "";
                    }
                    else
                    {
                        ExecutionTime = "Elapsed: " + arithmeticTestState.Result.Item1.ToString();
                        Result = "Result: " + arithmeticTestState.Result.Item2;
                    }
                });
        }

        private string testDescription;
        public string TestDescription
        {
            get => testDescription;
            set
            {
                testDescription = value;
                OnPropertyChanged();
            }
        }

        private bool isBusy;
        public bool IsBusy
        {
            get => isBusy;
            set
            {
                isBusy = value;
                OnPropertyChanged();
            }
        }

        private string executionTime;
        public string ExecutionTime
        {
            get => executionTime;
            set
            {
                executionTime = value;
                OnPropertyChanged();
            }
        }

        private string result;
        public string Result
        {
            get => result;
            set
            {
                result = value;
                OnPropertyChanged();
            }
        }

        protected void ExecuteTest()
        {
            IsBusy = true;
            ExecutionTime = string.Empty;
            Result = string.Empty;
            Task.Run(() => appStore.DispatchAsync(baseTestActionCreator.ExecuteTest()));
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}