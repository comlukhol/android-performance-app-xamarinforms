﻿using PerformanceXamForms.Model.State;
using Redux;
using PerformanceXamForms.Model;
using PerformanceXamForms.Redux.ActionCreators;

namespace PerformanceXamForms.ViewModels
{
    public class LocationViewModel : BaseTestViewModel
    {
        public LocationViewModel(IStore<ApplicationState> appStore, LocationActionCreator locationActionCreator) 
            : base(appStore, locationActionCreator, TestCode.READ_LAT_LNG)
        {
            TestDescription = "Odczytywanie współrzędnych urządzenia z GPS";
        }
    }
}