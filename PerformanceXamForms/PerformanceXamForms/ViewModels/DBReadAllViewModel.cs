﻿using PerformanceXamForms.Model;
using PerformanceXamForms.Model.State;
using PerformanceXamForms.Redux.ActionCreators;
using Redux;

namespace PerformanceXamForms.ViewModels
{
    public class DBReadAllViewModel : BaseTestViewModel
    {
        public DBReadAllViewModel(IStore<ApplicationState> appStore, DBReadAllActionCreator dbReadAllActionCreator)
            : base(appStore, dbReadAllActionCreator, TestCode.LOAD_FROM_DATABASE)
        {
            TestDescription = "Load all from database";
        }
    }
}
