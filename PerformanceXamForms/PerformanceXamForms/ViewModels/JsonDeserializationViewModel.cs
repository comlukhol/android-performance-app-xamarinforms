﻿using PerformanceXamForms.Model;
using PerformanceXamForms.Model.State;
using PerformanceXamForms.Redux;
using PerformanceXamForms.Redux.ActionCreators;
using Redux;
using System;
using System.Reactive.Linq;

namespace PerformanceXamForms.ViewModels
{
    public class JsonDeserializationViewModel : BaseTestViewModel
    {
        public JsonDeserializationViewModel(IStore<ApplicationState> appStore, JsonDeserializationActionCreator jsonDeserializationActionCreator) 
            : base(appStore, jsonDeserializationActionCreator, TestCode.JSON_DESERIALIZATION)
        {
            TestDescription = "Test deserializaji napisów w formie jsona do obiektów. Ten sam string jest deserializowany 10 000 razy. Użyta biblioteka to Json Newtonsoft.";
        }
    }
}