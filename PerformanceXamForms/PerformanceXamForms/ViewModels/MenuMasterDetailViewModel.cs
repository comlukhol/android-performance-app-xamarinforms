﻿using PerformanceXamForms.Pages;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace PerformanceXamForms.ViewModels
{
    public class MenuMasterDetailViewModel
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private const string ICON = "icon.png";

        private List<MasterPageItem> masterPageItems;
        public List<MasterPageItem> MasterPageItems
        {
            get => masterPageItems;
            set { masterPageItems = value; OnPropertyChanged(); }
        }

        public MenuMasterDetailViewModel()
        {
            masterPageItems = new List<MasterPageItem>
            {
                new MasterPageItem("Arithmetic", typeof(ArithmeticPage), ICON),
                new MasterPageItem("Json serialization", typeof(JsonSerializationPage), ICON),
                new MasterPageItem("Json deserialization", typeof(JsonDeserializationPage), ICON),
                new MasterPageItem("Read file from device", typeof(ReadFromDevicePage), ICON),
                new MasterPageItem("Save file to device", typeof(SaveToDevicePage), ICON),
                new MasterPageItem("Bulk database insert", typeof(DBBulkInsertPage), ICON),
                new MasterPageItem("Read all from database", typeof(DBReadAllPage), ICON),
                new MasterPageItem("Load device location", typeof(LocationPage), ICON),
                new MasterPageItem("Rest queries", typeof(RestQuerisPage), ICON)
            };
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}