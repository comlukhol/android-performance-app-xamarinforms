﻿using PerformanceXamForms.Model.Dto;
using System.Threading.Tasks;

namespace PerformanceXamForms.WebServices
{
    public interface IWeatherService
    {
        Task<WeatherDto> GetWeatherAsync(string city);
    }
}