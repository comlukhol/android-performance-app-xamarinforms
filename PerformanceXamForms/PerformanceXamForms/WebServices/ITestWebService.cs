﻿using PerformanceXamForms.Model.Dto;
using System.Threading.Tasks;

namespace PerformanceXamForms.WebServices
{
    public interface ITestWebService
    {
        Task sendSimpleTestResult(SimpleTestDto simpleTestDto);
    }
}