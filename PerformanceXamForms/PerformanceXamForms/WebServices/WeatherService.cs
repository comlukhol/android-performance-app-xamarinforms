﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PerformanceXamForms.Model.Dto;

namespace PerformanceXamForms.WebServices
{
    public class WeatherService : IWeatherService
    {
        private readonly JsonSerializer camelCaseJsonSerializer;

        public WeatherService(JsonSerializer camelCaseJsonSerializer)
        {
            this.camelCaseJsonSerializer = camelCaseJsonSerializer;
        }

        public async Task<WeatherDto> GetWeatherAsync(string city)
        {
            try
            {
                Uri uri = new Uri(string.Format("https://api.openweathermap.org/data/2.5/weather?q={0},pl&appid=a4d092435de7aa0a03d195383861eb7f", city));

                using (var httpClient = new HttpClient())
                {
                    var response = await httpClient.GetStringAsync(uri);
                    var jObject = JObject.Parse(response);
                    return jObject.ToObject<WeatherDto>();
                }
            }
            catch (Exception e)
            {
                //Bad practice - empty catch
            }

            return null;
        }
    }
}