﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PerformanceXamForms.Model.Dto;

namespace PerformanceXamForms.WebServices
{
    public class TestWebService : ITestWebService
    {
        private readonly JsonSerializer camelCaseJsonSerializer;

        public TestWebService(JsonSerializer camelCaseJsonSerializer)
        {
            this.camelCaseJsonSerializer = camelCaseJsonSerializer;
        }

        public async Task sendSimpleTestResult(SimpleTestDto simpleTestDto)
        {
            try
            {
                Uri uri = new Uri(App.SERVER_URL + "/api/v1/test/save");

                using (var httpClient = new HttpClient())
                {
                    var jObject = JObject.FromObject(simpleTestDto, camelCaseJsonSerializer);
                    var content = new StringContent(jObject.ToString(), Encoding.UTF8, "application/json");
                    var response = await httpClient.PostAsync(uri, content);
                }
            }
            catch(Exception e)
            {
                //Bad practice - empty catch
            }
        }
    }
}