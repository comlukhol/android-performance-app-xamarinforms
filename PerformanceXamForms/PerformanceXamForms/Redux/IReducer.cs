﻿using Redux;
using System;
using System.Collections.Generic;
using System.Text;

namespace PerformanceXamForms.Redux
{
    public interface IReducer<T>
    {
        T Reduce(T previousState, IAction action);
    }
}
