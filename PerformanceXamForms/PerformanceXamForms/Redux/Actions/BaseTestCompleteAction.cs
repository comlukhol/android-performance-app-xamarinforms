﻿using PerformanceXamForms.Model;
using Redux;
using System;

namespace PerformanceXamForms.Redux.Actions
{
    public class BaseTestCompleteAction : IAction
    {
        public bool IsSuccess { get; private set; }
        public TestCode TestCode { get; private set; }
        public Tuple<long, string> Result { get; private set; }

        public BaseTestCompleteAction(TestCode testCode, bool isSuccess, Tuple<long, string> result)
        {
            TestCode = testCode;
            IsSuccess = isSuccess;
            Result = result;
        }
    }
}