﻿using Redux;

namespace PerformanceXamForms.Redux.Actions
{
    public class TestIdSetAction : IAction
    {
        public string TestId { get; private set; }

        public TestIdSetAction(string testId)
        {
            TestId = testId;
        }
    }
}