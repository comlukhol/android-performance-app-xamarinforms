﻿using PerformanceXamForms.Model;
using Redux;

namespace PerformanceXamForms.Redux.Actions
{
    public class BaseTestStartAction : IAction
    {
        public TestCode TestCode { get; private set; }

        public BaseTestStartAction(TestCode testCode)
        {
            TestCode = testCode;
        }
    }
}