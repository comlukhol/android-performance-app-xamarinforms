﻿using PerformanceXamForms.Model;
using PerformanceXamForms.Model.State;
using PerformanceXamForms.Redux.Actions;
using Redux;
using System.Collections.Generic;

namespace PerformanceXamForms.Redux
{
    public class ApplicationReducer : IReducer<ApplicationState>
    {
        public ApplicationState Reduce(ApplicationState previousState, IAction action)
        {
            if(action is TestIdSetAction)
            {
                var testIdSetAction = action as TestIdSetAction;
                return new ApplicationState(testIdSetAction.TestId, previousState.TestStatesDictionary);
            }

            if(action is BaseTestStartAction)
            {
                var arithmeticTestStartAction = action as BaseTestStartAction;
                var previousDictionary = previousState.TestStatesDictionary;
                previousDictionary[arithmeticTestStartAction.TestCode] = new TestState(arithmeticTestStartAction.TestCode, true, false, null);

                return new ApplicationState(
                    previousState.TestId,
                    new Dictionary<TestCode, TestState>(previousDictionary)
                );
            }

            if(action is BaseTestCompleteAction)
            {
                var baseTestCompleteAction = action as BaseTestCompleteAction;
                var previousDictionary = previousState.TestStatesDictionary;
                previousDictionary[baseTestCompleteAction.TestCode] = new TestState(
                    TestCode.ARITHMETIC, false, baseTestCompleteAction.IsSuccess, baseTestCompleteAction.Result
                );

                return new ApplicationState(
                   previousState.TestId,
                   new Dictionary<TestCode, TestState>(previousDictionary)
               );
            }

            return previousState;
        }
    }
}