﻿using PerformanceXamForms.Common;
using PerformanceXamForms.Model;
using PerformanceXamForms.Tests;
using PerformanceXamForms.WebServices;

namespace PerformanceXamForms.Redux.ActionCreators
{
    public class ReadFromDeviceActionCreator : BaseTestActionCreator
    {
        public ReadFromDeviceActionCreator(ITestWebService testWebService, ILog log, ReadFromDeviceTest readFromDeviceTest) :
            base(testWebService, log, readFromDeviceTest, TestCode.READ_FROM_DEVICE)
        {

        }
    }
}