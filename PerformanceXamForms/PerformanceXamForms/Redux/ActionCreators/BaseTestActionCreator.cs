﻿using PerformanceXamForms.Common;
using PerformanceXamForms.Model;
using PerformanceXamForms.Model.State;
using PerformanceXamForms.Redux.Actions;
using PerformanceXamForms.Tests;
using PerformanceXamForms.WebServices;

namespace PerformanceXamForms.Redux.ActionCreators
{
    public class BaseTestActionCreator
    {
        protected readonly ITestWebService testWebService;
        protected readonly ILog log;
        protected readonly ITest test;
        protected readonly TestCode testCode;

        public BaseTestActionCreator(ITestWebService testWebService, ILog log, ITest test, TestCode testCode)
        {
            this.testWebService = testWebService;
            this.log = log;
            this.test = test;
            this.testCode = testCode;
        }

        public StoreExtensions.AsyncActionCreator<ApplicationState> ExecuteTest()
        {
            return async (dispatch, getState) =>
            {
                dispatch(new BaseTestStartAction(testCode));

                var resultTuple = await test.Execute();

                if (!string.IsNullOrEmpty(getState().TestId))
                {
                    await testWebService.sendSimpleTestResult(new Model.Dto.SimpleTestDto()
                    {
                        TestId = getState().TestId,
                        TestCode = testCode,
                        time = resultTuple.Item1
                    });

                    log.EmmitFinish();
                }

                dispatch(new BaseTestCompleteAction(testCode, resultTuple != null, resultTuple));
            };
        }
    }
}
