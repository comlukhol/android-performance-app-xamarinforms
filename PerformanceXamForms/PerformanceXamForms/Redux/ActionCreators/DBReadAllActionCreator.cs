﻿using PerformanceXamForms.Common;
using PerformanceXamForms.Model;
using PerformanceXamForms.Tests;
using PerformanceXamForms.WebServices;

namespace PerformanceXamForms.Redux.ActionCreators
{
    public class DBReadAllActionCreator : BaseTestActionCreator
    {
        public DBReadAllActionCreator(ITestWebService testWebService, ILog log, DbReadAllTest dbReadAllTest) 
            : base(testWebService, log, dbReadAllTest, TestCode.LOAD_FROM_DATABASE)
        {
        }
    }
}
