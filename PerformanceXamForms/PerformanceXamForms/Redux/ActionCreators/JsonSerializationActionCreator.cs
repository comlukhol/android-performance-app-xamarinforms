﻿using PerformanceXamForms.Common;
using PerformanceXamForms.Model;
using PerformanceXamForms.Tests;
using PerformanceXamForms.WebServices;

namespace PerformanceXamForms.Redux.ActionCreators
{
    public class JsonSerializationActionCreator : BaseTestActionCreator
    {
        public JsonSerializationActionCreator(ITestWebService testWebService, ILog log, JsonSerializationTest jsonSerializationTest)
            : base(testWebService, log, jsonSerializationTest, TestCode.JSON_SERIALIZATION)
        {
            
        }
    }
}