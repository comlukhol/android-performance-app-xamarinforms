﻿using PerformanceXamForms.Common;
using PerformanceXamForms.Model;
using PerformanceXamForms.Tests;
using PerformanceXamForms.WebServices;

namespace PerformanceXamForms.Redux.ActionCreators
{
    public class DBBulkInsertActionCreator : BaseTestActionCreator
    {
        public DBBulkInsertActionCreator(ITestWebService testWebService, ILog log, DBBulkInsertTest dbBulkInsertTest) 
            : base(testWebService, log, dbBulkInsertTest, TestCode.SAVE_TO_DATABASE)
        {

        }
    }
}