﻿using PerformanceXamForms.Common;
using PerformanceXamForms.Model;
using PerformanceXamForms.Tests;
using PerformanceXamForms.WebServices;

namespace PerformanceXamForms.Redux.ActionCreators
{
    public class ArithmeticActionCreator : BaseTestActionCreator
    {
        public ArithmeticActionCreator(ITestWebService testWebService, ILog log, ArithmeticTest arithmeticTest)
            : base(testWebService, log, arithmeticTest, TestCode.ARITHMETIC)
        {
           
        }
    }
}