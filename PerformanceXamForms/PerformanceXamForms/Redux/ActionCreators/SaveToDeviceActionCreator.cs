﻿using PerformanceXamForms.Common;
using PerformanceXamForms.Model;
using PerformanceXamForms.Tests;
using PerformanceXamForms.WebServices;

namespace PerformanceXamForms.Redux.ActionCreators
{
    public class SaveToDeviceActionCreator : BaseTestActionCreator
    {
        public SaveToDeviceActionCreator(ITestWebService testWebService, ILog log, SaveToDeviceTest test)
            : base(testWebService, log, test, TestCode.SAVE_TO_DEVICE)
        {
        }
    }
}
