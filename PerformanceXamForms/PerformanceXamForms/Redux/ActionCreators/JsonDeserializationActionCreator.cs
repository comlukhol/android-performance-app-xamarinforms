﻿using PerformanceXamForms.Common;
using PerformanceXamForms.Model;
using PerformanceXamForms.Tests;
using PerformanceXamForms.WebServices;

namespace PerformanceXamForms.Redux.ActionCreators
{
    public class JsonDeserializationActionCreator : BaseTestActionCreator
    {
        public JsonDeserializationActionCreator(ITestWebService testWebService, ILog log, JsonDeserializationTest jsonDeserializationTest)
            : base(testWebService, log, jsonDeserializationTest, TestCode.JSON_DESERIALIZATION)
        {

        }
    }
}