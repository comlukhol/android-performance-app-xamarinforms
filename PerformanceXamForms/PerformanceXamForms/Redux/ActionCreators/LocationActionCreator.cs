﻿using PerformanceXamForms.Common;
using PerformanceXamForms.Model;
using PerformanceXamForms.Tests;
using PerformanceXamForms.WebServices;
using System;
using System.Collections.Generic;
using System.Text;

namespace PerformanceXamForms.Redux.ActionCreators
{
    public class LocationActionCreator : BaseTestActionCreator
    {
        public LocationActionCreator(ITestWebService testWebService, ILog log, LocationTest arithmeticTest)
           : base(testWebService, log, arithmeticTest, TestCode.READ_LAT_LNG)
        {

        }
    }
}