﻿using PerformanceXamForms.Common;
using PerformanceXamForms.Model;
using PerformanceXamForms.Tests;
using PerformanceXamForms.WebServices;

namespace PerformanceXamForms.Redux.ActionCreators
{
    public class RestQuerisActionCreator : BaseTestActionCreator
    {
        public RestQuerisActionCreator(ITestWebService testWebService, ILog log, RestQuerisTest restQuerisTest) 
            : base(testWebService, log, restQuerisTest, TestCode.REST_QUERIES)
        {

        }
    }
}