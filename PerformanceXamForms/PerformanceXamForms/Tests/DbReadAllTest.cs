﻿using PerformanceXamForms.Common;
using PerformanceXamForms.Model.Entity;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace PerformanceXamForms.Tests
{
    public class DbReadAllTest : ITest
    {
        private readonly IDatabase database;

        public DbReadAllTest(IDatabase database)
        {
            this.database = database ?? throw new ArgumentNullException(nameof(database));
            database.Init();
        }

        public async Task<Tuple<long, string>> Execute()
        {
            database.InsertAll<SampleEntity>(PrepareSampleEntities());

            Stopwatch sw = new Stopwatch();
            sw.Start();

            var allEntities = database.GetAll<SampleEntity>("sample_entity");

            sw.Stop();

            database.Clear<SampleEntity>();

            return new Tuple<long, string>(sw.ElapsedMilliseconds, "");
        }

        private List<SampleEntity> PrepareSampleEntities()
        {
            List<SampleEntity> sampleEntities = new List<SampleEntity>();

            for (int i = 0; i < 10000; i++)
            {
                sampleEntities.Add(new SampleEntity()
                {
                    BooleanContent = true,
                    DoubleContent = 1515.114465654,
                    StringContent = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin vel malesuada urna. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aenean varius tincidunt eleifend. Sed semper urna vehicula condimentum consequat. Proin in malesuada nisi. Nam et pretium arcu, ut rutrum felis. Nunc laoreet pharetra nisl luctus rutrum. Mauris ex odio, facilisis et ultricies vel, gravida eu ante. Phasellus bibendum ex eu pellentesque egestas. Etiam nec dictum sapien, eget molestie lectus. Aenean lorem leo, commodo ut sodales et, rutrum eu quam. Proin gravida, ex vitae tincidunt viverra, orci augue consectetur quam, nec dignissim nunc diam egestas lorem. Nullam rutrum cursus diam, eu pretium odio elementum eu. Nam quis consequat diam. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed faucibus accumsan nibh, quis faucibus elit tempor sed. Maecenas id felis a lectus euismod volutpat. Sed gravida mollis tristique. Donec nec gravida urna. Nunc nec urna in odio volutpat accumsan. Suspendisse vel odio metus."
                });
            }

            return sampleEntities;
        }
    }
}
