﻿using System;
using System.Threading.Tasks;

namespace PerformanceXamForms.Tests
{
    public interface ITest
    {
        Task<Tuple<long, string>> Execute();
    }
}