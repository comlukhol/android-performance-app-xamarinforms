﻿using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceXamForms.Tests
{
    public class LocationTest : ITest
    {
        public async Task<Tuple<long, string>> Execute()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            var locator = CrossGeolocator.Current;
            locator.DesiredAccuracy = 100;
            Position position = null;

            try
            {
                position = await locator.GetPositionAsync();
            }
            catch (Exception e)
            {
                var data = e.Data;
            }

            var result = string.Format("Latitude: {0}, Longitude: {1}", position.Latitude, position.Longitude);
            sw.Stop();

            return new Tuple<long, string>(sw.ElapsedMilliseconds, result);
        }
    }
}
