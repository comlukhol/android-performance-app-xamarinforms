﻿using System;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceXamForms.Tests
{
    public class ArithmeticTest : ITest
    {
        public async Task<Tuple<long, string>> Execute()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            string result = PiDigit(35000);

            sw.Stop();

            return new Tuple<long, string>(sw.ElapsedMilliseconds, result);
        }

        private double FirstArithmeticTest()
        {
            double x = 1;
            for (int i = 0; i < 1000000; i++)
            {
                x += (i * 0.99);
                for (int j = 0; j < 100; j++)
                {
                    x += 0.01;
                }
            }

            return x;
        }

        private long ForLoopOverTenMilionWithAddition()
        {
            long sum = 0;
            for (int i = 0; i < 10000000; i++)
            {
                sum += i;
            }

            return sum;
        }

        private long ForLoopOverMaxInteger()
        {
            long sum = 0;
            for (int i = 0; i < int.MaxValue; i++)
            {
                sum += i;
            }

            return sum;
        }

        private string PiDigit(int digits)
        {
            const int SCALE = 10000;
            const int ARRINIT = 2000;

            StringBuilder pi = new StringBuilder();
            int[] arr = new int[digits + 1];
            int carry = 0;

            for (int i = 0; i <= digits; ++i)
                arr[i] = ARRINIT;

            for (int i = digits; i > 0; i -= 14)
            {
                int sum = 0;
                for (int j = i; j > 0; --j)
                {
                    sum = sum * j + SCALE * arr[j];
                    arr[j] = sum % (j * 2 - 1);
                    sum /= j * 2 - 1;
                }

                pi.Append(string.Format("{0}", carry + sum / SCALE));
                carry = sum % SCALE;
            }
            return pi.ToString();
        }
    }
}