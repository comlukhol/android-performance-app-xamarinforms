﻿using PCLStorage;
using PerformanceXamForms.Common;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace PerformanceXamForms.Tests
{
    public class ReadFromDeviceTest : ITest
    {
        private readonly IStorageService storageService;

        public ReadFromDeviceTest(IStorageService storageService)
        {
            this.storageService = storageService ?? throw new ArgumentNullException(nameof(storageService));
        }

        public async Task<Tuple<long, string>> Execute()
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            byte[] bytes;
            for(int i = 0; i < 100 ; i++)
            {
                bytes = await storageService.GetFileBytesAsync("/sdcard/performanceTests/photos/android-photo-small.jpg");
            }

            stopwatch.Stop();

            return new Tuple<long, string>(stopwatch.ElapsedMilliseconds, "Read from device.");
        }
    }
}
