﻿using PerformanceXamForms.Model.Dto;
using PerformanceXamForms.WebServices;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace PerformanceXamForms.Tests
{
    public class RestQuerisTest : ITest
    {
        private IWeatherService weatherService;

        public RestQuerisTest(IWeatherService weatherService)
        {
            this.weatherService = weatherService ?? throw new ArgumentNullException(nameof(weatherService));
        }

        public async Task<Tuple<long, string>> Execute()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            WeatherDto weatherDto = null;
            for(int i = 0; i < 5; i++)
            {
                weatherDto = await weatherService.GetWeatherAsync("Lodz");
            }
            sw.Stop();

            return new Tuple<long, string>(sw.ElapsedMilliseconds, weatherDto.ToString());
        }
    }
}
