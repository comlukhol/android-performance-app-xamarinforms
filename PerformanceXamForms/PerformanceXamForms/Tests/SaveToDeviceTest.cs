﻿using PerformanceXamForms.Common;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace PerformanceXamForms.Tests
{
    public class SaveToDeviceTest : ITest
    {
        private IStorageService storageService;
        private byte[] fileBytes;
        private string fileBase64String;

        public SaveToDeviceTest(IStorageService storageService)
        {
            this.storageService = storageService ?? throw new ArgumentNullException(nameof(storageService));
        }

        public async Task<Tuple<long, string>> Execute()
        {
            await Prepare();

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            for(int i = 0; i < 100; i++)
            {
                //await SaveString("/sdcard/performanceTests/photos/write/android-photo-small-" + i + ".txt", fileBase64String);
                await storageService.SaveBytesAsync("/sdcard/performanceTests/photos/write/android-photo-small-" + i + ".jpg", fileBytes);
            }

            stopwatch.Stop();

            return new Tuple<long, string>(stopwatch.ElapsedMilliseconds, "Save to device SUCCESS.");
        }

        private async Task Prepare()
        {
            await storageService.DeleteFolderIfExistAsync("/sdcard/performanceTests/photos/write/");
            await storageService.CreateFolderAsync("/sdcard/performanceTests/photos/write/");

            fileBytes = await storageService.GetFileBytesAsync("/sdcard/performanceTests/photos/android-photo-small.jpg");
            fileBase64String = Convert.ToBase64String(fileBytes);
        }
    }
}
