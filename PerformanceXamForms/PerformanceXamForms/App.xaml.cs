﻿using PerformanceXamForms.Common;
using PerformanceXamForms.Model.State;
using PerformanceXamForms.Redux.Actions;
using Redux;
using Xamarin.Forms;
using PerformanceXamForms.Pages;
using System.Threading.Tasks;
using PerformanceXamForms.Redux.ActionCreators;
using PerformanceXamForms.Redux;
using Plugin.Permissions.Abstractions;

namespace PerformanceXamForms
{
	public partial class App : Application
	{
        public static string SERVER_URL = "https://360d58ea.ngrok.io";
        private readonly ILog log = DIContainer.Instance.Resolve<ILog>();
        private readonly INavigationService navigationService = DIContainer.Instance.Resolve<INavigationService>();
        private readonly IStore<ApplicationState> appStore = DIContainer.Instance.Resolve<IStore<ApplicationState>>();
        private readonly IPermissionManager permissionManager = DIContainer.Instance.Resolve<IPermissionManager>();

        public App(string inputParam = null, string inputParameterTwo = null)
        {
            InitializeComponent();
            appStore.Dispatch(new TestIdSetAction(inputParam));
            SERVER_URL = inputParameterTwo;

            permissionManager.CheckPermissionsAsync(Permission.Storage, Permission.Camera);
            MainPage = navigationService.InitNavigation<ArithmeticPage>();

            if (inputParam != null)
                AutomaticMode();
        }

        private void AutomaticMode()
        {
            Task.Run(async () =>
            {
                await Task.Delay(1000);
                await appStore.DispatchAsync(DIContainer.Instance.Resolve<ArithmeticActionCreator>().ExecuteTest());

                await Task.Delay(1000);
                await navigationService.NavigateToFromTask(typeof(JsonSerializationPage));
                await Task.Delay(1000);
                await appStore.DispatchAsync(DIContainer.Instance.Resolve<JsonSerializationActionCreator>().ExecuteTest());

                await Task.Delay(1000);
                await navigationService.NavigateToFromTask(typeof(JsonDeserializationPage));
                await Task.Delay(1000);
                await appStore.DispatchAsync(DIContainer.Instance.Resolve<JsonDeserializationActionCreator>().ExecuteTest());

                await Task.Delay(1000);
                await navigationService.NavigateToFromTask(typeof(ReadFromDevicePage));
                await Task.Delay(1000);
                await appStore.DispatchAsync(DIContainer.Instance.Resolve<ReadFromDeviceActionCreator>().ExecuteTest());

                await Task.Delay(1000);
                await navigationService.NavigateToFromTask(typeof(SaveToDevicePage));
                await Task.Delay(1000);
                await appStore.DispatchAsync(DIContainer.Instance.Resolve<SaveToDeviceActionCreator>().ExecuteTest());

                await Task.Delay(1000);
                await navigationService.NavigateToFromTask(typeof(DBBulkInsertPage));
                await Task.Delay(1000);
                await appStore.DispatchAsync(DIContainer.Instance.Resolve<DBBulkInsertActionCreator>().ExecuteTest());

                await Task.Delay(1000);
                await navigationService.NavigateToFromTask(typeof(DBReadAllPage));
                await Task.Delay(1000);
                await appStore.DispatchAsync(DIContainer.Instance.Resolve<DBReadAllActionCreator>().ExecuteTest());

                await Task.Delay(1000);
                await navigationService.NavigateToFromTask(typeof(LocationPage));
                await Task.Delay(1000);
                await appStore.DispatchAsync(DIContainer.Instance.Resolve<LocationActionCreator>().ExecuteTest());

                await Task.Delay(1000);
                await navigationService.NavigateToFromTask(typeof(RestQuerisPage));
                await Task.Delay(1000);
                await appStore.DispatchAsync(DIContainer.Instance.Resolve<RestQuerisActionCreator>().ExecuteTest());

                appStore.Dispatch(new TestIdSetAction(null));
                log.EmmitFinish();
            });
        }

		protected override void OnStart ()
		{

		}

		protected override void OnSleep ()
		{

        }

        protected override void OnResume ()
		{

        }
    }
}