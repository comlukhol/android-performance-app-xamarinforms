﻿using System.Threading.Tasks;

namespace PerformanceXamForms.Common
{
    public interface IPictureStorage
    {
        Task SavePictureAsync(string filename, byte[] imageBytes);
    }
}
