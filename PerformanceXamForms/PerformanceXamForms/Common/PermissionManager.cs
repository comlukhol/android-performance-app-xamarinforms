﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;

namespace PerformanceXamForms.Common
{
    public class PermissionManager : IPermissionManager
    {
        public async Task CheckPermissionsAsync(params Permission[] permissions)
        {
            List<Permission> notGrantedPermissions = new List<Permission>();
            foreach(Permission permission in permissions)
            {
                var status = await CrossPermissions.Current.CheckPermissionStatusAsync(permission);
                if (status != PermissionStatus.Granted)
                    notGrantedPermissions.Add(permission);
            }

            if(notGrantedPermissions.Count != 0)
                await CrossPermissions.Current.RequestPermissionsAsync(notGrantedPermissions.ToArray());
        }
    }
}
