﻿using PerformanceXamForms.Common;
using PerformanceXamForms.ViewModels;
using PerformanceXamForms.Model.State;
using PerformanceXamForms.Redux;
using Redux;
using SimpleInjector;
using Xamarin.Forms;
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using PerformanceXamForms.WebServices;
using PerformanceXamForms.Tests;
using System.Collections.Generic;
using PerformanceXamForms.Model;
using PerformanceXamForms.Redux.ActionCreators;
using PerformanceXamForms.Pages;

namespace PerformanceXamForms.Common
{
    public class DIContainer
    {
        private static readonly Container simpleInjectorContainer = new Container();
        private static readonly DIContainer instance = new DIContainer();

        public static DIContainer Instance
        {
            get => instance;
        }

        protected DIContainer()
        {
            simpleInjectorContainer.Register<IReducer<ApplicationState>, ApplicationReducer>();
            simpleInjectorContainer.Register(() => PrepareInitialApplicationState());
            simpleInjectorContainer.Register<IStore<ApplicationState>>(() =>
            {
                return new Store<ApplicationState> (
                    Resolve<IReducer<ApplicationState>>().Reduce,
                    Resolve<ApplicationState>()
                );
            }, Lifestyle.Singleton);

            simpleInjectorContainer.Register<RootMasterDetailPage>();

            //Common
            simpleInjectorContainer.Register(() => CreateCameCaseJsonSerializer(), Lifestyle.Singleton);
            simpleInjectorContainer.Register<ILog>(() => DependencyService.Get<ILog>(), Lifestyle.Singleton);
            simpleInjectorContainer.Register<IPictureStorage>(() => DependencyService.Get<IPictureStorage>(), Lifestyle.Singleton);
            simpleInjectorContainer.Register<INavigationService, NavigationService>(Lifestyle.Singleton);
            simpleInjectorContainer.Register<ITestWebService, TestWebService>(Lifestyle.Singleton);
            simpleInjectorContainer.Register<IPermissionManager, PermissionManager>(Lifestyle.Singleton);
            simpleInjectorContainer.Register<IDatabase, Database>(Lifestyle.Singleton);
            simpleInjectorContainer.Register<IStorageService, StorageService>(Lifestyle.Singleton);
            simpleInjectorContainer.Register<IWeatherService, WeatherService>(Lifestyle.Singleton);

            try
            {
                simpleInjectorContainer.Verify();
            }
            catch(Exception e)
            {
                var str = e.Message;
            }
        }

        public T Resolve<T>()
        {
            return (T)simpleInjectorContainer.GetInstance(typeof(T));
        }

        public object Resolve(Type type) 
        {
            return simpleInjectorContainer.GetInstance(type);
        }

        private ApplicationState PrepareInitialApplicationState()
        {
            var testsStatesDictionary = new Dictionary<TestCode, TestState>();

            //Tests initialization here:
            foreach (TestCode testCode in (TestCode[])Enum.GetValues(typeof(TestCode)))
                AddTestState(testCode, testsStatesDictionary);

            return new ApplicationState(null, testsStatesDictionary);
        }

        private JsonSerializer CreateCameCaseJsonSerializer()
        {
            var jsonSerializer = new JsonSerializer();
            jsonSerializer.ContractResolver = new CamelCasePropertyNamesContractResolver();
            return jsonSerializer;
        }

        private void AddTestState(TestCode testCode, IDictionary<TestCode, TestState> testsStatesDictionary)
        {
            testsStatesDictionary.Add(testCode, new TestState(testCode, false, false, null));
        }
    }
}