﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PerformanceXamForms.Common
{
    public interface INavigationService
    {
        Page InitNavigation<T>() where T : Page;
        Task NavigateTo(Type pageType, bool animation = false);
        Task NavigateToRoot(Type pageType, bool animation = false);
        Task NavigateToFromTask(Type pageType, bool animation = false);
        Task NavigateToRootFromTask(Type pageType, bool animation = false);
        Task<Page> NavigateBack();
        Task<Page> NavigateBackRoot();
    }
}