﻿using Plugin.Permissions.Abstractions;
using System.Threading.Tasks;

namespace PerformanceXamForms.Common
{
    public interface IPermissionManager
    {
        Task CheckPermissionsAsync(params Permission[] permissions);
    }
}