﻿using System.Threading.Tasks;

namespace PerformanceXamForms.Common
{
    public interface IStorageService
    {
        Task DeleteFolderIfExistAsync(string path);
        Task CreateFolderAsync(string path);
        Task SaveBytesAsync(string path, byte[] bytes);
        Task SaveStringAsync(string path, string content);
        Task<byte[]> GetFileBytesAsync(string path);
    }
}
