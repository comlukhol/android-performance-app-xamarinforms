﻿using PCLStorage;
using System;
using System.Threading.Tasks;

namespace PerformanceXamForms.Common
{
    public class StorageService : IStorageService
    {
        private IPictureStorage pictureStorage;

        public StorageService(IPictureStorage pictureStorage)
        {
            this.pictureStorage = pictureStorage ?? throw new ArgumentNullException(nameof(pictureStorage));
        }

        public async Task CreateFolderAsync(string path)
        {
            await FileSystem.Current.LocalStorage.CreateFolderAsync(path, CreationCollisionOption.ReplaceExisting);
        }

        public async Task DeleteFolderIfExistAsync(string path)
        {
            var existFolder = await FileSystem.Current.LocalStorage.CheckExistsAsync(path);
            if (existFolder == ExistenceCheckResult.FolderExists)
            {
                var folder = await FileSystem.Current.LocalStorage.GetFolderAsync(path);
                await folder.DeleteAsync();
            }
        }

        public async Task<byte[]> GetFileBytesAsync(string path)
        {
            var file = await FileSystem.Current.LocalStorage.GetFileAsync(path);
            var stream = await file.OpenAsync(PCLStorage.FileAccess.Read);
            var bytes = new byte[stream.Length];
            await stream.ReadAsync(bytes, 0, (int)stream.Length);
            return bytes;
        }

        public async Task SaveBytesAsync(string path, byte[] bytes)
        {
            await pictureStorage.SavePictureAsync(path, bytes);
        }

        public async Task SaveStringAsync(string path, string content)
        {
            var file = await FileSystem.Current.LocalStorage.CreateFileAsync(path, CreationCollisionOption.ReplaceExisting);
            await file.WriteAllTextAsync(content);
        }
    }
}
