﻿using System.Collections.Generic;

namespace PerformanceXamForms.Common
{
    public interface IDatabase
    {
        void Init();
        void InsertAll<T>(List<T> sampleEntities);
        List<T> GetAll<T>(string tableName) where T : new();
        void Clear<T>();
        void Destroy();
    }
}