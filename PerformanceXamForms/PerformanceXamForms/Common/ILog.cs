﻿namespace PerformanceXamForms.Common
{
    public interface ILog
    {
        void Verbose(string TAG, string message);
        void Info(string TAG, string message);
        void Debug(string TAG, string message);
        void Error(string TAG, string message);
        void Warn(string TAG, string message);
        void EmmitFinish();
    }
}
