﻿using PerformanceXamForms.Model.Entity;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;

namespace PerformanceXamForms.Common
{
    public class Database : IDatabase
    {
        private SQLiteConnection connection;

        public void Init()
        {
            if (connection != null)
                return;

            var databasePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "android-performance-tests.db");
            connection = new SQLiteConnection(databasePath);

            connection.DropTable<SampleEntity>();
            connection.CreateTable<SampleEntity>();
        }

        public void InsertAll<T>(List<T> sampleEntities)
        {
            connection.InsertAll(sampleEntities);
        }

        public List<T> GetAll<T>(string tableName) where T : new()
        {
            var query = "SELECT * FROM " + tableName;
            return connection.Query<T>(query);
        }

        public void Destroy()
        {
            if(connection != null)
                connection.DropTable<SampleEntity>();
        }

        public void Clear<T>()
        {
            connection.DeleteAll<T>();
        }
    }
}
