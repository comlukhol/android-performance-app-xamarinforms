﻿using PerformanceXamForms.Pages;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PerformanceXamForms.Common
{
    public class NavigationService : INavigationService
    {
        private CustomNavigationPage rootNavigationPage;
        private CustomNavigationPage innerNavigationPage;
        private CustomNavigationPage outerNavigationPage;
        private RootMasterDetailPage rootMasterDetailPage;

        public NavigationService()
        {

        }

        public Page InitNavigation<T>() where T : Page
        {
            var rootMasterDetailPage = DIContainer.Instance.Resolve<RootMasterDetailPage>();
            var firstPage = (Page)DIContainer.Instance.Resolve(typeof(T));

            this.innerNavigationPage = new CustomNavigationPage(firstPage);
            this.outerNavigationPage = new CustomNavigationPage(innerNavigationPage);
            rootMasterDetailPage.Detail = outerNavigationPage;
            this.rootNavigationPage = new CustomNavigationPage(rootMasterDetailPage);
            this.rootMasterDetailPage = rootMasterDetailPage;

            NavigationPage.SetHasNavigationBar(firstPage, false);
            NavigationPage.SetHasNavigationBar(rootMasterDetailPage, false);

            innerNavigationPage.Title = firstPage.Title;

            //Change toolbar title (need to be done manually)
            innerNavigationPage.Popped += (a, e) =>
            {
                innerNavigationPage.Title = innerNavigationPage.Navigation.NavigationStack.Last().Title;
            };

            return rootNavigationPage;
        }

        public Task<Page> NavigateBack()
        {
            if (innerNavigationPage.Navigation.NavigationStack.Count > 1)
                return innerNavigationPage.PopAsync();

            throw new Exception("Cannot pop if only one page is on navigation stack!");
        }

        public Task<Page> NavigateBackRoot()
        {
            if (rootNavigationPage.Navigation.NavigationStack.Count > 1)
                return rootNavigationPage.Navigation.PopAsync();

            throw new Exception("Cannot pop if only one page is on navigation stack!");
        }

        public async Task NavigateTo(Type pageType, bool animation = false)
        {
            if (IsPageOnTop(pageType))
                return;

            var page = (Page)DIContainer.Instance.Resolve(pageType);
            NavigationPage.SetHasNavigationBar(page, false);
            await innerNavigationPage.PushAsync(page, animation);
            innerNavigationPage.Title = page.Title;
        }

        public async Task NavigateToFromTask(Type pageType, bool animation = false)
        {
            if (IsPageOnTop(pageType))
                return;

            var page = (Page)DIContainer.Instance.Resolve(pageType);
            NavigationPage.SetHasNavigationBar(page, false);

            Thread functionThread = Thread.CurrentThread;
            Device.BeginInvokeOnMainThread(async () =>
            {
                await innerNavigationPage.PushAsync(page, animation);
                innerNavigationPage.Title = page.Title;
                functionThread.Interrupt();
            });

            try
            {
                Thread.Sleep(2000);
            }
            catch (Exception e)
            {

            }
        }

        public async Task NavigateToRoot(Type pageType, bool animation = false)
        {
            if (rootNavigationPage.Navigation.NavigationStack.Last().GetType() == pageType)
                return;

            var page = (Page)DIContainer.Instance.Resolve(pageType);
            innerNavigationPage.Title = page.Title;
            await rootNavigationPage.Navigation.PushAsync(page, animation);
        }

        public async Task NavigateToRootFromTask(Type pageType, bool animation = false)
        {
            if (IsPageOnTopRoot(pageType))
                return;

            var page = (Page)DIContainer.Instance.Resolve(pageType);

            Thread functionThread = Thread.CurrentThread;
            Device.BeginInvokeOnMainThread(async () =>
            {
                await rootNavigationPage.PushAsync(page, animation);
                functionThread.Interrupt();
            });

            try
            {
                Thread.Sleep(2000);
            }
            catch (Exception e)
            {

            }
        }

        private bool IsPageOnTop(Type pageType)
        {
            var innerPageType = innerNavigationPage.Navigation.NavigationStack.Last().GetType();
            return innerPageType == pageType;
        }

        private bool IsPageOnTopRoot(Type pageType)
        {
            var innerPageType = rootNavigationPage.Navigation.NavigationStack.Last().GetType();
            return innerPageType == pageType;
        }
    }
}